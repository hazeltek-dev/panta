from pyramid.view import view_config


@view_config(route_name='home', renderer='index.html')
def index(request):
    return {}


def includeme(config):
    '''Pyramid includeme hook.

    :param config: app config
    :type config: :class:`pyramid.config.Configurator`
    '''
    config.add_static_view('static-panta', 'panta:static')

    # Jinja2 templates as .html files
    config.include('pyramid_jinja2')
    config.add_jinja2_renderer('.html')
    config.add_jinja2_renderer('.txt')

    # add core templates to search path
    pkg_path = 'panta:templates'
    config.add_jinja2_search_path(pkg_path, name='.html')
    config.add_jinja2_search_path(pkg_path, name='.txt')
    config.scan(__name__)
