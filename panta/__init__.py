import pkg_resources
from pyramid.config import Configurator
from pyramid.util import DottedNameResolver


## all of these can be set by passing them in the
## Paste Deploy settings:
conf_defaults = {
    'panta.base_includes': ' '.join([
        'panta',
        'panta.views',
        'panta.routes'
    ])
}

conf_dotted = {}


def get_version():
    '''Returns the version details for Panta
    '''
    return pkg_resources.require('Panta')[0].version


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    print(settings)
    config = _panta_configure(global_config, **settings)
    return config.make_wsgi_app()


def _resolve_dotted(d, keys=conf_dotted):
    resolved = d.copy()
    for key in keys:
        value = resolved[key]
        if not isinstance(value, str):
            continue
        new_value = []
        for dottedname in value.split():
            new_value.append(DottedNameResolver().resolve(dottedname))
        resolved[key] = new_value
    return resolved


def _panta_configure(global_config, **settings):
    # resolve dotted names in settings, include plugins & create a Configurator
    for key, value in conf_defaults.items():
        settings.setdefault(key, value)
    
    # allow extending packages to change 'settings' with python
    key = 'panta.configurators'
    for func in _resolve_dotted(settings, keys=(key,))[key]:
        func(settings)

    # we'll process ``pyramid_includes`` later to allow overrides of
    # configuration from ``panta.base_includes``
    pyramid_includes = settings.pop('pyramid.includes', '')

    config = Configurator(settings=settings)
    config.begin()

    # apply pyramid_includes
    config.registry.settings['pyramid.includes'] = pyramid_includes

    # include modules listed in 'panta.base_includes':
    for module in settings['panta.base_includes'].split():
        config.include(module)

    config.commit()

    # modules in 'pyramid.includes' may override 'panta.base_includes'
    if pyramid_includes:
        for module in pyramid_includes.split():
            config.include(module)

    config.commit()
    return config


def includeme(config):
    '''Pyramid includeme hook.

    :param config: app config
    :type config: :class:`pyramid.config.Configurator`
    '''
    return config
